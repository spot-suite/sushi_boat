# Introduction

[Sushi Boat](https://gitlab.com/spot-suite/sushi_boat) is a bash shell based
workflow framework emphasizing a separation between the major components of a
workflow application:
1. Workflow engine: The Sushi Boat engine itself
2. Workflow logic: The application's workflow structure
3. Workflow task: The details of each step in the workflow

A given application using the framework writes the 2nd and 3rd of these.

This separation allows development and testing of the individual workflow tasks
to be done separately from changes to the workflow logic structure and vice
versa.

Sushi Boat additionally provides separate modules for customizing task
submission and registration mechanisms.  Current task submission is done via
shell backgrounding with submitting to a Rabbit Message Queue planned as a
future addition.  Registration is currently made to the SPOT mongo DB, though
this can be overridded to write to a file.

# Quick Start
To get started and run the included demonstration workflow:

```
git clone git@gitlab.com:spot-suite/sushi_boat.git
cd sushi_boat
./demo/demo_wfl.sh
```

This will start the demonstration workflow as defined by the files under the
`./demo/` subdirectory.  Some basic log information will be printed to stdout
with detailed logs under the `ENG_WORK_DIR` location shown.  Running the demo
should take from 20 to 30 seconds.

# Overview
The Sushi Boat framework consist of 4 components:
1. The Sushi Boat **Engine**: The underlying driver that runs your workflow.
2. The **[Workflow Definition](#workflow-definition)**: The script that defines
   the logic of your workflow and a **[Run Script](#run-script)** to start your
   workflow within the Sushi Boat framework.
3. The **[Harnesses](#harness-scripts)**: The scripts that bind the workflow to
   the individual tasks.
4. The **[Tasks](#task-scripts)**: The scripts that execute the individual steps
   of the workflow.

To use Sushi Boat, the user writes the last 3 of these.  The underlying
philosophy is that the harness scripts encapsulate the workflow and engine
knowledge from the task scripts which should be able to be run in a stand-alone
manner outside of a workflow system.

The demo workflow is an example of this.  Details about each of these are in the
following sections.

## Workflow Definition

A Workflow is defined by implementing two functions `initial_tasks` and
`task_succeeded`.  See the `demo_wfl_defn.sh` script as an example.

The `initial_tasks` function is called to launch all of the tasks needed to
start a workflow whereas the `task_succeeded` function is called whenever a task
completes so any subsequent tasks can be started

Tasks are started by using the `launch_task` function that takes two arguments:
the label for the task and a harness script that will setup and invoke that
task.

```shell
initial_tasks() {
    launch_task "TASK1" /path/to/harness_for_TASK1.sh
    launch_task "TASK2" /path/to/harness_for_TASK2.sh
}
```

Note that the harness scripts *do not* take any arguments.  This is by design as
the harness scripts are intented to encapsulate all the information they need to
run their task.  (Details on how this is done is described below in the
**[Harnesses](#harness-scripts)** section.)

Further, there exists a `launch_mi_task` that takes 3 arguments for starting
multiple instances of the same task.  The first two arguments are the same as
for `launch_task` with the 3rd being the number of instances to launch.  See
**[Launching Multiple Instance Tasks](#launching-multiple-instance-tasks)**
below for more details.

The `task_succeeded` function is called when *any* task started by the engine
has completed successfully.  This hook is then used to launch the next task or
set of tasks in the workflow.

```shell
task_succeeded() {
    local succeeded=${1}  # The label of the task that just completed

    # After TASK1, launch TASK3
    if completes_task "${succeeded}" "TASK1" ; then
      launch_task "TASK3" /path/to/harness_for_TASK3
    fi

    # After TASK2 completes, launch TASK4
    if completes_task "${succeeded}" "TASK2" ; then
      launch_task "TASK4" /path/to/harness_for_TASK4
    fi
}
```

## Workflow Control Patterns

The above example shows how to have one task follow another - this is the
sequence workflow control pattern.  Below is the complete list of supported
workflow control patterns, with examples of how to add them to a workflow
definition.

* *[Sequential](#sequence)*: one task begins execution
  when its preceding task has successfully completed.
* *[Fork](#fork)*: two of more tasks begin execution in
  parallel when their preceding task has successfully completed.
* *[Join](#join)*: two or more tasks all need to
  successfully complete before the succeeding task can begin.
* *[Split](#split)*: one task out of a group of tasks,
  based on a condition, is selected to begin execution when the
  preceding task has successfully completed.
* *[Merge](#merge)*: any one of a group of tasks needs to
  successfully complete before the succeeding task can begin.

These can be implemented within the `task_succeeded` function by using the
supplied `completes_task`, `completes_join` and `completes_merge` functions as
demonstrated below.

### Sequence
```shell
# After A, launch B
if completes_task ${succeeded} "A" ; then
    launch_task "B" /path/to/harness_B.sh
fi
```

### Fork
```shell
# After C, launch both D and E
if completes_task ${succeeded} "C" ; then
    launch_task "D" /path/to/harness_D.sh
    launch_task "E" /path/to/harness_E.sh
fi
```

### Join
```shell
# After both D and E have completed, launch F
if completes_join ${succeeded} "D" "E" ; then
    launch_task "F" /path/to/harness_F.sh
fi
```

### Split
```shell
# After G, launch either H or I, depending on condition
if completes_join ${succeeded} "G" ; then
    if [ condition  ]; then
        launch_task "H" /path/to/harness_H.sh
    else
        launch_task "I" /path/to/harness_I.sh
    fi
fi
```

### Merge
```shell
# After either H or I has completed, launch J
if completes_merge ${succeeded} "H" "I" ; then
    launch_task "J" /path/to/harness_J.sh
fi
```

### Launching Multiple Instance tasks

While not a workflow control pattern, the following is how one can launch
multiple instances of the same task.

```shell
# Launch 3 instances of task K
launch_mi_task "K" /path/to/harness_K.sh 3
```

Note that this is nearly identical to how the `launch_task` function is called
except for the 3rd argument which specifies how many instances to launch.
Additionally, each instance will have defined in their environment two
variables:
1. `ARRAY_SIZE` : The number of instances of this multiple instance task that
   have been launched. (The 3rd arg to `launch_mi_task`).
2. `ARRAY_INDEX` : The unique instance number for the given task within the
   multiple instance task.  The first one will be `0` the last one will be
   `$ARRAY_SIZE - 1`.

Within the workflow, this is considered a single task.  So any of the above
control patterns can be used with multiple instance tasks as they would be with
single instance tasks.

## Executing a Workflow

### Run Script

To start a run of your workflow within the Sushi Boat framework, a script needs
to be written that pulls in all the needed engine and workflow environment
variables, functions and starts the workflow.

The `demo_wfl.sh` script is an example of this.

### Harness scripts

The Harness scripts are used to isolate the engine and workflow details from the
task scripts.  Harness scripts are called in the workflow definition file and
should not take any arguments, in the spirit of keeping workflow logic details
separate from the task scripts they wrap.  The demo workflow harness scripts
show how a common `harness_env.sh` script can be sourced by each of the harness
scripts to discover needed information for launching the task scripts.

### Task Scripts

The Task scripts are used to launch an individual task within the workflow.
Ideally these scripts are isolated from engine and workflow details and could be
run stand-alone outside of the system for development, testing and debugging
purposes.

### Workflow Environment

The following environmental variables can be used define the environment in
which is will execute. If the variable does not exist either a default value
will be used or a default behavior executed.  See the `demo_wfl.sh` and
`demo_env.sh` files for examples of setting these variables.

* `ENG_DEBUG` : must be non-null for move verbose engine log output to be
  generated.  If this is not defined only info level log output will be
  generated.

* `ENG_DIR` : location of the `engine` directory in this project.  This _must_
  be set in the *[Run Script](#run-script)* file (the file used to execute the
  workflow) so all of the workflow engine's components can be found.

* `ENG_RUN_ID` : the id for a given run of the engine.  Defaults to $HOSTNAME
  (or `engine` if HOSTNAME is not set) followed by the value of $WFL_ID (or the
  current pid if $WFL_ID is not set).

* `ENG_WORK_DIR` : the "work area" for a given instance of the workflow.  This
  defaults to `${SCRATCH}/sushi_work/${ENG_RUN_ID}` or
  `$(pwd)/sushi_work/${ENG_RUN_ID}`

* `REG_LOG_FILE` : location of the file into which all registration information
  should be written.  If this is not defined then all registration information
  will be written into the `mongodb`.

* `SUSHI_MONGO` : location of the configuration file specifying how to connect
  to the `mongodb` instance into which all registration information will be
  written.  If this is not defined then the engine will attempt to use the
  `${HOME}/.sushi_mongo` file, and if that does not exist then the default SPOT
  `mongodb` will be used.
