#!/bin/bash

# OS specific flags
case $(uname -s) in
    Linux)
        #module load python/2.7-anaconda-2019.07
        # Don't source activate!
        # It's slow and not reentrant
        PATH=~/.conda/envs/sushi_boat/bin:$PATH
        ;;
    Darwin)
        ;;
    FreeBSD)
        ;;
esac
