#!/usr/bin/env python

from time import localtime, gmtime, strftime
import os
try:
    reg_log_file=os.environ['REG_LOG_FILE']
except KeyError:
    reg_log_file=None
if None == reg_log_file:
    import pymongo
import sys

def err(str): 
    print >> sys.stderr, strftime("%a %b %d %H:%M:%S PDT %Y ", localtime()), "register_job.py:", str 

def log(str): 
    print >> sys.stdout, strftime("%a %b %d %H:%M:%S PDT %Y ", localtime()), "register_job.py:", str 

def writeToMongodb(values, configuation):
    try:
        connection=None
        connection=pymongo.MongoClient(**configuration)
        db=connection.alsdata

        ret=db.authenticate("alsdata","Sch3m@t1c5")

        if ret == False:
            err("unable to authenticate to mongo db ...")
            sys.exit(1)

        collection=db.jobstatus

        doc=None
        found=False

        if operation == "update":
            doc=collection.find_one({'jobid':jobid})

        if doc is None:
            doc={'system':'carver'}
        else:
            found=True

        for key in values.iterkeys():
            doc[key]=values[key]

        if found  == True:
            ret=collection.update_one({'jobid':jobid}, doc)
        else:
            ret=collection.insert_one(doc)

        log(ret)
    finally:
        if connection != None:
            log("Closing connection ...")
            connection.close()

def writeToFile(file, values):
    with open(file, 'a') as f:
        conjunction="{   "
        for key in values.iterkeys():
            pair=conjunction+key+":"+str(values[key])
            f.write(pair)
            conjunction=""",
    """
        f.write("""
}
""")

try:
    sushi_config=os.environ['SUSHI_MONGO']
except KeyError:
    sushi_config=os.path.join(os.environ['HOME'],
                              '.sushi_mongo')

configuration={}
if os.path.isfile(sushi_config):
    with open("file.txt") as f:
        for line in f:
            (key, val) = line.split()
            if 'host' != key:
                configuration[key] = int(val)
            else:
                configuration[key] = val
else:
    configuration['host']='mongodb01.nersc.gov'
    configuration['port']=27017
    configuration['socketTimeoutMS']=600000
    configuration['connectTimeoutMS']=120000

if len(sys.argv) < 7:
    err("invalid arguments ... " + str(len(sys.argv)) + ":" + ', '.join(sys.argv) )
    sys.exit(1)

operation=sys.argv[1]

if operation != "update" and operation != "insert":
    err("operation not supported:" + operation)
    sys.exit(1)

status=sys.argv[2]
h5file=sys.argv[3]
h5fileSize=int(sys.argv[4])

jobid=sys.argv[5] 
stage=sys.argv[6]

facility=""

if len(sys.argv) >= 8:
    facility=sys.argv[7]

endstation=""

if len(sys.argv) >= 9:
    endstation=sys.argv[8]

owner=""
if len(sys.argv) >= 10:
    owner=sys.argv[9]

dataset=""

logs=""
ttime=-1
qtime=-1
wtime=-1
outfile=""
outfileSize=-1

if len(sys.argv) >= 11:
    dataset=sys.argv[10]

    if status != "INFO" and status != "WARN":
        try:
            fname=os.environ['ENG_WORK_DIR'] + "/" + stage+dataset.replace("/", "_")+".register.txt"
            print "Looking for file", fname, stage

            if os.path.isfile(fname):
                wtime=0
                wcounter=0

                print "Found file", fname, stage
                fo=open(fname, "r")
                line=fo.readline()

                while line:
                    logs+=line
                    idx=line.find("QUEUE_TIME=")
                    if idx != -1:
                        qtime=max(qtime, int(line[idx+len("QUEUE_TIME="):].strip()))
                    else:
                        idx=line.find("WALL_TIME=")

                        if idx != -1:
                            wtime+=int(line[idx+len("WALL_TIME="):].strip())
                            wcounter+=1
                        else:
                            idx=line.find("TOTAL_TIME=")

                            if idx != -1:
                                ttime=max(ttime, int(line[idx+len("TOTAL_TIME="):].strip()))
                    line = fo.readline()

                if wcounter != 0:
                    wtime=wtime/wcounter
                else:
                    wtime=-1

                fo.close()
            else:
                print "NOT Found file", fname, stage
                log (fname + " does not exist.")
        except Exception as detail:
            logs=""
            ttime=-1
            qtime=-1
            wtime=-1
            err(detail)

num=-1

if len(sys.argv) >=  12:
    num=int(sys.argv[11])

parent=""

if len(sys.argv) >= 13:
    parent=sys.argv[12]

resources=-1
if len(sys.argv) >= 14:
    print "NODES/CORES", stage, sys.argv[13]
    resources=int(sys.argv[13])

if len(sys.argv) >= 15:
    outfile=sys.argv[14]

if len(sys.argv) >=  16:
    outfileSize=int(sys.argv[15])

if operation == "update":
    log("update status={0},h5file={1},jobid={2},stage={3},fac={4},station={5},owner={6},set={7},num={8},parent={9}".format(status, h5file, jobid, stage, facility, endstation, owner, dataset, num, parent))
elif operation == "insert":
    log("insert status={0},h5file={1},jobid={2},stage={3},fac={4},station={5},owner={6},set={7},num={8},parent={9}".format(status, h5file, jobid, stage, facility, endstation, owner, dataset, num, parent))
else:
    err("operation not supported:" + operation)
    sys.exit(1)

connection=None
code=0

values={}
values['jobid']=jobid
values['jobtype']=stage
values['facility']=facility
values['endstation']=endstation
values['owner']=owner
values['dataset']=dataset
values['status']=status
values['parent']=parent
values['resources']=resources
values['h5RawFile']=h5file
values['h5RawFileSize']=h5fileSize
values['numsets']=num
values['date']=strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())
values['logs']=logs
values['qtime']=qtime
values['wtime']=wtime
values['ttime']=ttime
values['outfile']=outfile
values['outfileSize']=outfileSize

try:
    if None==reg_log_file:
        writeToMongodb(values, configuration)
    else:
        writeToFile(reg_log_file, values)
except Exception as detail:
    code = 1
    err("oops")
    err(detail)
    err("(what a pain!)")

sys.exit(code)
