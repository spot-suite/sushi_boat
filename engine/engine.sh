# -*- mode: sh; sh-shell: bash; -*-

####
# Variables
#
# Naming conventions:
#   - Uppercase means it is exported and can be used by workflow.
#
#   - Lowercase means not exported and internal (this file: engine
#     only, not indended for workflow)
#
#   - Preceeding `_` means internal to engine and should not be
#     modified by workflow
#
#   - Containing "ENG_" or "eng_" means it is used and/or set by engine and
#     shoud not be modified by workflow
#
#   - Containing "WFL_" or "wfl_" are vars set by the workflow for
#   - use, if set, by the engine.
####

_script=$(basename ${BASH_SOURCE[0]})
_script_dir=$(cd $(dirname ${BASH_SOURCE[0]}); pwd -P)

# Set sushi git version if not already set
: ${ENG_VERSION:=$(cd ${_script_dir} && git describe --tags --dirty --always)}
export ENG_VERSION

# This should already be set but if not...
: ${ENG_DIR:=${_script_dir}}
export ENG_DIR

####
# Functions
####

function eng.init() {
    # Initialize engine variables, directories, etc.  Should be run
    # one, before any other functions herein are called.
    
    # The ID used for this run of the engine
    : ${ENG_RUN_ID:=${HOSTNAME:=engine}.${WFL_ID:=$$}}
    export ENG_RUN_ID

    # For the scratch dir, use $SCRATCH if defined otherwise use pwd
    : ${sushi_scratch:=${SCRATCH:=$(pwd)}}

    # The working dir in which the engine and workflow will operate
    : ${ENG_WORK_DIR:=${sushi_scratch}/sushi_work/${ENG_RUN_ID}}
    export ENG_WORK_DIR
    mkdir -p ${ENG_WORK_DIR}

    # Engine log file named after script, unless already set
    : ${ENG_LOG:=${ENG_WORK_DIR}/$( basename ${0} ".sh").log}
    export ENG_LOG

    # Below is subject to removing (moving out of engine into
    # workflows) in issue #13
    : ${WFL_H5_PATH:="/path/to/datafile.h5"}
    : ${WFL_FACILITY:="sb_facility"}
    : ${WFL_END_STATION:="sb_end_station"}
    : ${WFL_OWNER:="sb_owner"}
    : ${WFL_NUM_OF_DATASETS:=1}
    : ${WFL_DATASET:=$(basename ${WFL_H5_PATH} ".h5")}
    export WFL_DATASET  # exported because both the engine and execute_task()
                        # uses this in the naming of the registration and
                        # summary files

    wfl_h5_filename=$(basename ${WFL_H5_PATH})
    wfl_h5_filesize=$(get_filesize ${WFL_H5_PATH})

    register_txt="${ENG_WORK_DIR}/driver_${WFL_DATASET}.register.txt"

    export PATH=${ENG_DIR}:${PATH}
}


function initiate_task() {
    local label=${1}
    local command=${2}
    local cardinality=${3}

    # Register that we've submitted the job, nothing will be drawn
    registerJob ${ENG_RUN_ID} submit-${label} $WFL_FACILITY $WFL_END_STATION $WFL_OWNER /${WFL_DATASET} $WFL_NUM_OF_DATASETS
    export HARNESS_START_DATE=$(date +%s)

    # do submission!
    sub.submit_task ${label} ${command} ${cardinality}
    ret=${?}

    # Register the success or failure of submission of the job.  If
    # failure, line will be shown otherwise nothing is shown.
    registerStatus "${ENG_RUN_ID} submit-${label} $WFL_FACILITY $WFL_END_STATION $WFL_OWNER /${WFL_DATASET} $WFL_NUM_OF_DATASETS" "$ret"

    if [ 0 == ${ret} ] ; then
        # Creates a "Running" line for the job in "The Box"
        id=${ENG_RUN_ID}.${label}
        registerJob $id ${label} $WFL_FACILITY $WFL_END_STATION $WFL_OWNER /${WFL_DATASET} $WFL_NUM_OF_DATASETS ${ENG_RUN_ID} 1

        if [ -n "${pending}" ] ; then
            pending="${pending} ${label}"
        else
            pending=${label}
        fi
    fi
}

launch_mi_task() {
  eng.dbg "Launching mutliple-instances of \"${1}\" using \"${2}\""
  echo ${3} > ${ENG_WORK_DIR}/${1}_${WFL_DATASET}.cardinality
  initiate_task "${1}" "${2}" "${3}"
}

launch_task() {
  eng.dbg "Launching instance of \"${1}\" using \"${2}\""
  initiate_task "${1}" "${2}" "N"
}

function completes_task() {
  local succeeded=${1}
  local target=${2}
  [ "${succeeded}" == "${target}" ]
}

function get_filesize() {
    local file=${1}
    local stat_flags=""

    # OS specific flags for stat
    case $(uname -s) in
        Linux)   stat_flags="-c %s";;
        Darwin)  stat_flags="-f %z";;
        FreeBSD) stat_flags="-f %c";;
    esac

    if [ -f "${file}" ]; then
        stat ${stat_flags} ${file}        
    else
        echo "-1"
    fi
}

function get_summary_base() {
    local label=${1}
    echo ${ENG_WORK_DIR}/${label}_${WFL_DATASET}.summary
}


function get_ret_filename() {
    local label=${1}
    echo ${ENG_WORK_DIR}/${label}_${WFL_DATASET}.ret
}

function check_succeeded() {
    # Return true/false if the file containing the return code of
    # given task exists indicating that the engine has seen the
    # completion of that task.
    local label=${1}
    local ret_filename=$(get_ret_filename ${label})
    test -f ${ret_filename}
}

function completes_join() {
    local succeeded=${1}
    shift
    local regex="(^| )${succeeded}($| )"  # match words, not substrings
    if [[ "$@" =~ ${regex} ]] ; then
        eng.dbg "Testing join of \"${@}\" after \"${succeeded}\" has succeeded"
        for target in ${@} ; do
            if completes_task ${succeeded} ${target} ; then
                :
            else
                if check_succeeded ${target} ; then
                    :
                else
                    eng.dbg "Waiting for join of \"${@}\" because \"${target}\" is pending"
                    return 255
                fi
            fi
        done
        eng.dbg "Successful join of \"${@}\""
        return 0
    fi
    return 255
}

function completes_merge() {
    local succeeded=${1}
    shift
    if [[ "$@" == *"${succeeded}"* ]] ; then
        eng.dbg "Successful merge of \"${succeeded}\""
        return 0
    fi
    return 255
}

function check_task() {
    # Check for the completion of the specified task returning the
    # return status of that task and echoing the string "incomplete"
    # or "completed".  Completion is determined if the expected number
    # of summary files have been seen or not.

    local label=${1}
    eng.dbg "check_task: Checking success of \"${label}\""

    # If the return status file already exist, use it.
    local ret_filename=$(get_ret_filename ${label})
    if [ -f ${ret_filename} ]; then
        echo "completed"
        return $(cat ${ret_filename})
    fi

    local summary_base=$(get_summary_base ${label})
    local cardinality_file=${ENG_WORK_DIR}/${label}_${WFL_DATASET}.cardinality

    if [ -f ${cardinality_file} ] ; then
        summary_expected=`cat ${cardinality_file}`
    else
        summary_expected=-1
    fi
    summary_found=`ls -1 ${summary_base}* 2> /dev/null | wc -l`
    if [ ${summary_expected} == -1 ]; then    # Single instance
        if [ ${summary_found} != 1 ]; then
            eng.dbg "check_task: Still waiting for summary file for \"${label}\""
            echo "incomplete"
            return 255
        fi
    elif [ ${summary_expected} != ${summary_found} ] ; then  # Multiple instance
        eng.dbg "check_task: Still waiting for $((${summary_expected} - ${summary_found})) multiple instance summary files for \"${label}\""
        echo "incomplete"
        return 255
    fi
    local task_ret=0
    if [ 1 == ${summary_found} ] ; then
        task_ret=$(cat ${summary_base} | grep code | awk ' { print $6 }')
    else
        local returns=( $(cat ${summary_base}* | grep code | awk ' { print $6 }') )
        for loop_ret in "${returns[@]}" ; do
            if [ 0 != ${loop_ret} -a 0 == ${task_ret} ]; then
                 task_ret=${loop_ret}
            fi
        done
    fi

    # Write task's return code to a file named after the task that
    # signals that the engine has seen the end of that task.
    echo ${task_ret} > ${ret_filename}
    eng.dbg "check_task: Return code for \"${label}\" is: \"${task_ret}\""

    # Write summary files into register file for later insertion into
    # mongo, them remove them.
    cat ${summary_base}* 2> /dev/null >> ${ENG_WORK_DIR}/${label}_${WFL_DATASET}.register.txt
    rm -f ${summary_base}*

    id=${ENG_RUN_ID}.${label}
    registerStatus "${id} ${label} $WFL_FACILITY $WFL_END_STATION $WFL_OWNER /${WFL_DATASET} $WFL_NUM_OF_DATASETS ${ENG_RUN_ID} 1" "${task_ret}"
    echo "completed"
    return ${task_ret}
}

# Internal log function
function _eng.log() {
    local level=${1}
    shift
    local src=$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]}-2]})  # we're 2 files in
    local line=$(basename ${BASH_LINENO[${#BASH_LINENO[@]}-3]}) # we're 3 funcs in
    local elapsed=" [0s]:"
    if [[ -n "${eng_stime}" ]]; then
        elapsed=" [$(( $(date '+%s') - ${eng_stime} ))s]:"
    fi
    echo "$(date):${elapsed} ${level}: sushi_boat: ${src}:${line}: $@" >> ${ENG_LOG}        
}

function eng.log() {
    _eng.log "info" "$@"
}

function eng.dbg() {
    if [ "X" != "X${ENG_DEBUG}" ]; then
        _eng.log "debug" "$@"
    fi
}

function registerJob() {
    registerJob.sh insert INFO ${wfl_h5_filename} ${wfl_h5_filesize} "$@" 2>> ${ENG_LOG}
}

function registerStatus() {
    local bar=(`echo $1`)

    if [ "$2" = "0" ]; then
        registerJob.sh insert OK ${wfl_h5_filename} ${wfl_h5_filesize} "${bar[@]}" 2>> ${ENG_LOG}
    else
        registerJob.sh insert FAIL ${wfl_h5_filename} ${wfl_h5_filesize} "${bar[@]}" 2>> ${ENG_LOG}
    fi
}

function registerSelf() {
    if [ -n "${ENG_RUN_ID}" ]; then
        registerJob.sh update INFO ${wfl_h5_filename} ${wfl_h5_filesize} "$@" 2>> ${ENG_LOG}
    fi
}

function registerSelfStatus() {
    if [ -n "${ENG_RUN_ID}" ]; then
        local bar=(`echo $1`)

        if [ "$2" = 0 ]; then
            registerJob.sh insert OK ${wfl_h5_filename} ${wfl_h5_filesize} "${bar[@]}" 2>> ${ENG_LOG}
        else
            registerJob.sh insert FAIL ${wfl_h5_filename} ${wfl_h5_filesize} "${bar[@]}" 2>> ${ENG_LOG}
        fi
    fi
}


function eng.start() {
    eng.log "Sushi Boat Version: ${ENG_VERSION}"
    eng.log "Workflow Version: ${WFL_VERSION}"
    
    # Set overall engine start time
    eng_stime=$(date "+%s")
    
    eng.log "Sushi Boat Engine: run: ${ENG_RUN_ID}, start time: ${eng_stime}"

    WSTART_DATE=$(date +%s)
    POLLING_INTERVAL=5

    # Create "The Box" starting the processing of the dataset
    registerSelf ${ENG_RUN_ID} driver $WFL_FACILITY $WFL_END_STATION $WFL_OWNER /$WFL_DATASET

    # Start initial tasks (as defined by workflow) which will do initial
    # populating of $pending list
    initial_tasks
    wfl_ret=$?

    # Main workflow engine loop:
    # keep going until no jobs are pending
    running=${pending}
    while [ -n "${running}" ] ; do
        eng.log "polling the following tasks: ${running}"
        pending=""
        for task_label in ${running} ; do
            status=$(check_task ${task_label})
            task_ret=${?}
            if [ "incomplete" == ${status} ] ; then
                if [ -n "${pending}" ] ; then
                    pending="${pending} ${task_label}"
                else
                    pending=${task_label}
                fi
            elif [ 0 == ${task_ret} ] ; then
                eng.log "${task_label} completed successfully"
                task_succeeded ${task_label}
            else
                if [ 0 == ${wfl_ret} ] ; then
                    wfl_ret=${task_ret}
                fi
                eng.log "${task_label} failed with return code ${task_ret}"
            fi
        done
        if [ -n "${pending}" ] ; then
            eng.log "continuing to wait for the following tasks: ${pending}"
            sleep ${POLLING_INTERVAL}
        fi
        running=${pending}
    done

    summary_base=`get_summary_base driver`
    summary_temp=${summary_base}.temp
    eng_ftime=$(date +%s)
    echo "$(date):JOB=${ENG_RUN_ID}:-------------------------" >> ${summary_temp}
    local elapsed=$(( $WSTART_DATE - ${eng_stime}))
    echo "$(date):PERF:JOB=${ENG_RUN_ID}:EXIT_CODE=${wfl_ret}:QUEUE_TIME=${elapsed}" >> ${summary_temp}
    elapsed=$(( ${eng_ftime} - $WSTART_DATE ))
    echo "$(date):PERF:JOB=${ENG_RUN_ID}:EXIT_CODE=${wfl_ret}:WALL_TIME=${elapsed}" >> ${summary_temp}
    elapsed=$(( ${eng_ftime} - ${eng_stime} ))
    echo "$(date):PERF:JOB=${ENG_RUN_ID}:EXIT_CODE=${wfl_ret}:TOTAL_TIME=${elapsed}" >> ${summary_temp}

    # Save the return code of ourselves
    local ret_filename=$(get_ret_filename driver)
    echo ${wfl_ret} > ${ret_filename}

    mv ${summary_temp} ${summary_base}
    cat ${summary_base} >> ${register_txt}
    rm ${summary_base}

    # Finalize the run, ending the processing of the dataset
    registerSelfStatus "${ENG_RUN_ID} driver $WFL_FACILITY $WFL_END_STATION $WFL_OWNER /$WFL_DATASET $WFL_NUM_OF_DATASETS" "${wfl_ret}"

    # If workflow has not failed and ENG_DEBUG is not set, clean up
    # engine files
    if [[ ${wfl_ret} == 0 && -z ${ENG_DEBUG} ]]; then
        eng.log "Cleaning up summary, cardinality, register and ret files"
        rm -f ${ENG_WORK_DIR}/*.ret \
           ${ENG_WORK_DIR}/*.register.txt \
           ${ENG_WORK_DIR}/*.cardinality
    fi

    eng.log "Engine exiting with status: ${wfl_ret}"
    return ${wfl_ret}
}

unset _script _script_dir
