#
# Functions needed by the harness scripts.
#

#
# Calculates the slice indices across the given full length of a list
#   for multiple instance tasks.
# For example in the harness it will appear like this
#
#   range=( $( calculate_slice $1 $2 $3 ) )
#
# For a list the is not cleanly divisable the first slices will contain
#   an extra element. For example slicing 10 into 3 you will get
#   slices [0,4), [4, 7) and [7, 10).
#
# ToDo: Is this function needed here?  Seems like something specific
# to the workflow, not needed in the engine.
function calculate_slice() {
    local full_length=$1
    local array_index=$2
    local array_size=$3

    local base_count=$((full_length/array_size))
    local overflow_count=$((full_length%array_size))

    if [ ${array_index} -ge ${overflow_count} ] ; then
        begin=$(( ${array_index} * ${base_count} + ${overflow_count}))
        end=$(( ${begin} + ${base_count}))
    else
        begin=$(( ${array_index} * ${base_count} + ${array_index}))
        end=$(( ${begin} + ${base_count} + 1))
    fi

    a=($begin $end)
    echo "${a[@]}"
}


function execute_task() {
    local label=${1}
    local command=${2}
    local comm=$(basename ${command})
    shift 2
    local args="$@"

    local wstart_date=$(date +%s)

    echo "$(date):${comm}:${ENG_RUN_ID}:INFO:TASK=${label}:EXECUTION=${command} ${args}" >> \
        ${ENG_WORK_DIR}/${label}_${WFL_DATASET}.register.txt
    ${command} ${args}
    ret=${?}

    if [ -n "${ARRAY_INDEX}" ] ; then
        array_info=":ARRAY_INDEX=${ARRAY_INDEX}"
        array_suffix="-${ARRAY_INDEX}"
    fi
    summary_file=${ENG_WORK_DIR}/${label}_${WFL_DATASET}.temp${array_suffix}
    end_date=$(date +%s)
    echo "$(date):${comm}:${array_info}: -------------------------" >> ${summary_file}
    echo "Jobs exit status code is $ret" >> ${summary_file}
    local elapsed=$(( ${wstart_date} - $HARNESS_START_DATE))
    echo `date`:${comm}:"PERF:TASK=${label}${array_info}:EXIT_CODE=${ret}:QUEUE_TIME=${elapsed}" >> ${summary_file}
    elapsed=$(( ${end_date} - ${wstart_date} ))
    echo `date`:${comm}:"PERF:TASK=${label}${array_info}:EXIT_CODE=${ret}:WALL_TIME=${elapsed}" >> ${summary_file}
    elapsed=$(( ${end_date} - $HARNESS_START_DATE ))
    echo `date`:${comm}:"PERF:TASK=${label}${array_info}:EXIT_CODE=${ret}:TOTAL_TIME=${elapsed}" >> ${summary_file}

    # Registers the completion of the job, drawing an appropriate bar in "The Box"
    mv ${summary_file} ${ENG_WORK_DIR}/${label}_${WFL_DATASET}.summary${array_suffix}
    return ${ret}
}
