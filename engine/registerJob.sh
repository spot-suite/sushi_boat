#!/bin/bash

SCRIPT_NAME=$(basename $0)

# Pick up eng.log/dbg functions
. ${ENG_DIR}/engine.sh

eng.dbg "$@"

# Pick up sushi boat specific python env
source ${ENG_DIR}/pyenv.sh

START_DATE=$(date +%s)

python -Wd ${ENG_DIR}/register_job.py "$@" >> ${ENG_WORK_DIR}/register_job_py.txt
ret=$?

END_DATE=$(date +%s)
DIFF=$(( $END_DATE - $START_DATE ))

eng.dbg "DURATION=$DIFF EXIT_CODE=$ret"
exit $ret
