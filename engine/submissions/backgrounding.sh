#!/bin/bash

function sub.submit_task() {
    local label=${1}
    local command=${2}
    local cardinality=${3}

    ret=$?
    if [ "N" == ${cardinality} ] ; then
        nohup ${command} ${label} >> ${ENG_LOG} 2>&1 &
    _false_start_status $!
    ret=$?
    else
        export ARRAY_SIZE=${cardinality}
        for index in `seq 0 $((${cardinality} - 1))` ; do
            export ARRAY_INDEX=${index}
            nohup ${command} ${label} >> ${ENG_LOG} 2>&1 &
            _false_start_status $!
            tmp_ret=$?
            (( $tmp_ret > $ret )) && ret=$tmp_ret   # max status
            unset ARRAY_INDEX
        done
        unset ARRAY_SIZE
    fi
    return $ret
}


function _false_start_status() {
    # Look for false starts by using ps to see if the given pid is
    # running and if not use wait to get its return status.  If it
    # has finished, return that status, otherwise return 0.
    #
    # Note: The pid must be a process started within this shell as
    # 'wait' is used.
    pid=$1
    [ -z "$pid" ] && echo "false_start_status: error: missing pid" && return -1
    ret=0
    if ! ps -p $pid > /dev/null; then  # look for pid
        wait $pid                      # it's not there, get it's return status
        ret=$?
    fi
    return $ret
}
