#!/bin/bash

# Main entry point for launching the sushi boat demo workflow

script=$(basename ${BASH_SOURCE[0]})
script_dir=$(cd $(dirname ${BASH_SOURCE[0]}); pwd -P)

# Workflow vars exported to be used by engine and workflow tasks
export WFL_ID=demo.$$
export WFL_STIME=$(date +%s)

# Vars needed by the engine, specifically the registration system.
# See sushi_boat issue #13 for a better idea on how to avoid needing
# these in the demo.  For now set them to ignorable values here.
WFL_H5_PATH=/path/to/demo_wfl.h5
WFL_FACILITY=sb_facility
WFL_END_STATION=sb_end_station
WFL_OWNER=sb_owner
WFL_NUM_OF_DATASETS=1
WFL_DATASET=demo

# The demo workflow's top-level location.  This should be the only env
# var exported.  All others are sourced in or passed as argument
# values.
export WFL_HOME=$(cd ${script_dir}; pwd -P)

# Log registations to file, not mongo, see sushi_boat issue #13.
export REG_LOG_FILE=${WFL_HOME}/demo_wfl_reg.log

# Pick up top-level envs and functions
source ${WFL_HOME}/demo_env.sh

# Pick up demo's workflow definition
. ${WFL_HOME}/demo_wfl_defn.sh

# The submission to use
. ${ENG_DIR}/submissions/backgrounding.sh

# The engine itself
. ${ENG_DIR}/engine.sh
eng.init

wfl.log "Starting demo workflow with id: ${WFL_ID} and start time: ${WFL_STIME}"
wfl.log "ENG_WORK_DIR=${ENG_WORK_DIR}"

# Away we go...
eng.start
ret=$?

wfl.exit $ret
