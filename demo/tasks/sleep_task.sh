#!/bin/bash

# A generic "sleep" task called by all the demo tasks.

script=$(basename ${0})
script_dir=$(cd $(dirname ${0}); pwd -P)

if [[ $# -ne 2 ]]; then
    echo "Usage: ${script} task_name workflow_id"
    exit -1
fi

task_name=$1
workflow_id=$2

# Check if we're a multi-instance task
if [[ -n $ARRAY_SIZE ]]; then
    task_name="${task_name} (${ARRAY_INDEX} of ${ARRAY_SIZE})"
fi

# Pick up common task envs and functions
source ${script_dir}/task_env.sh

task.log "Starting ${task_name} with args: $@"

# Get time to work/sleep
sleep_time=$(task.get_sleep_time ${task_name} ${workflow_id})

# Give multi-instance tasks different sleep times
if [[ -n $ARRAY_SIZE ]]; then
    sleep_time=$(( $sleep_time * ($ARRAY_SIZE - $ARRAY_INDEX) ))
fi
            
task.log "Working on ${task_name} for ${sleep_time} sec"
sleep ${sleep_time}

task.exit $? "Task ${task_name} done"
