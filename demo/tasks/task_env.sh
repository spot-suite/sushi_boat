#-*- mode: sh; sh-shell: bash; -*-

# Common vars & functions used in the task scripts

# Note that task scripts should not need to know anything about the
# workflow.  Though information shared between tasks should go here.

_script=$(basename ${BASH_SOURCE[0]})
_script_dir=$(cd $(dirname ${BASH_SOURCE[0]}); pwd -P)

function task.log() {
    if [[ -n "${WFL_STIME}" ]]; then
        local elapsed=" [$(( $(date '+%s') - ${WFL_STIME} ))s]:"
    fi
    echo "$(date):${elapsed} ${script}: $@"
}

# Print something before exiting
# $1 exit status value
# $2, $3, ... optional extra messages to echo
function task.exit() {
    ret=$1
    if [[ -n "$2" ]]; then
        shift
        task.log $@
    fi
    task.log "Exiting with status: $ret"
    exit $ret
}

function task.get_sleep_time() {
    # Return the sleep time [0-8] for each task, using $1 as task
    # letter name and $2 as seed for PRN generator.
    #
    # The reason for this trickyness is to allow exact reproductions
    # of runs by setting the same wf_id.
    local task=$1
    local wf_id=$2

    local a_char=$(printf "%d" "'A")                # 'A' char value
    local task_char=$(printf "%d" "'${task}")       # task letter value
    local task_offset=$((${task_char} - ${a_char}))

    # Create an array of random numbs, seeded with wf_id
    RANDOM=${wf_id}   # seed bash's PRN generator
    local rands
    for ((i=0; i <= ${task_offset}; i++)); do
        rands[${i}]=$(($RANDOM % 8))
    done
    
    # return our numb
    echo ${rands[${task_offset}]}
}

unset _script _script_dir
