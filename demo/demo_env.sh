# -*- mode: sh; sh-shell: bash; -*-

# A top-level script to be sourced to define common vars & functions
# used across the demo workflow

_script=$(basename ${BASH_SOURCE[0]})
_script_dir=$(cd $(dirname ${BASH_SOURCE[0]}); pwd -P)

# Set the WFL_HOME var if it isn't already set.
: ${WFL_HOME:=$(cd ${_script_dir}; pwd -P)}

# Where the sushi boat engine lives, if not already set.
: ${ENG_DIR:=$(cd ${_script_dir}/../engine; pwd -P)}

# Where the harnesses live
WFL_HAR_DIR=${WFL_HOME}/harness

# Where the tasks live
WFK_TSK_DIR=${WFL_HOME}/tasks

# Log/debug functions for demo workflow
function _log() {
    # Internal: used only by wfl.log/wfl.dbg
    local level=${1}
    shift
    local src=$(basename ${BASH_SOURCE[${#BASH_SOURCE[@]}-1]})  # we're 1 file in
    local line=$(basename ${BASH_LINENO[${#BASH_LINENO[@]}-2]}) # We're 2 funcs in
    local elapsed=" [0s]:"
    if [[ -n "${WFL_STIME}" ]]; then
        elapsed=" [$(( $(date '+%s') - ${WFL_STIME} ))s]:"
    fi
    echo "$(date):${elapsed} ${level}: demo_wfl: ${src}:${line}: $@"
}

function wfl.log() {
    _log "info" "$@"
}

function wfl.dbg() {
    if [ "X" != "X${WFL_DEBUG}" ]; then
        _log "debug" "$@"
    fi
}

# Print something before exiting
# $1 exit status value
# $2, $3, ... optional extra messages to echo
function wfl.exit() {
    ret=$1
    if [[ -n "$2" ]]; then
        shift
        wfl.log $@
    fi
    wfl.log "Exiting with status: $ret"
    exit $ret
}

unset _script _script_dir
