#!/bin/bash

script=$(basename ${0})
script_dir=$(cd $(dirname ${0}); pwd -P)

# Internal label for tracking within the engine
sb_label=${1}

if [[ "$#" -ne 1 ]]; then
    echo "Usage: ${script}: sb_label"
    exit 1
fi

# Pick up demo harness env & functions
. ${script_dir}/harness_env.sh

wfl.log "Entering with args: $@"

# Use workflow timestamp as id (primarily to show a WFL var in use)
wf_id=${WFL_STIME}

# Generic task script
task_script="sleep_task.sh"

# Specific task name
task_name=J

# Run the task in the workflow
execute_task ${sb_label} ${WFK_TSK_DIR}/${task_script} ${task_name} ${wf_id}
ret=$?

wfl.exit ${ret}
