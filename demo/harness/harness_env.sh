#-*- mode: sh; sh-shell: bash; -*-

# Common vars & functions used in the harness scripts

# The purpose of the harness scripts is to isolate workflow knowledge
# from the task scripts.  Harness script set things up for the task
# scripts and call them with whatever args they need to run.  Ideally,
# task scripts can be run outside of the workflow.

_script=$(basename ${BASH_SOURCE[0]})
_script_dir=$(cd $(dirname ${BASH_SOURCE[0]}); pwd -P)

# Set the WFL_HOME var if it isn't already set.
: ${WFL_HOME:=$(cd ${_script_dir}/..; pwd -P)}

# Pick up top-level env
source ${WFL_HOME}/demo_env.sh

# Pick up SB task functions, i.e. execute_task
. ${ENG_DIR}/task_harness.sh

# Other harness specific things go here...


unset _script _script_dir
