#-*- mode: sh; sh-shell: bash; -*-

# The definition of the workflow to execute.
#
#             Sequence
#          +---+    +---+
#      +-->| A |--->| B |
#      |   +---+    +---+
#      |
#      |         Fork          Join
#      |                +---+
#      |           +--->| D |---+
#      |   +---+   |    +---+   |    +---+
#  *---+-->| C |---+            +--->| F |
#      |   +---+   |    +---+   |    +---+
#      |           +--->| E |---+
#      |                +---+
#      |
#      |         Split         Merge
#      |                +---+
#      |           +--->| H |---+
#      |   +---+   |    +---+   |    +---+
#      +-->| G |---+            +--->| J |
#      |   +---+   |    +---+   |    +---+
#      |           +--->| I |---+
#      |                +---+
#      |
#      | Multiple Instance
#      |   +------+
#      +-->| K{3} |-+    Sequence after MI
#          +------+ |-+      +---+
#            +------+ |----->| L |
#              +------+      +---+


# Define the initial tasks of the workflow, called at the very beginning
initial_tasks() {
    # Start A, C, G and K
    launch_task "A" ${WFL_HAR_DIR}/harness_A.sh
    launch_task "C" ${WFL_HAR_DIR}/harness_C.sh
    launch_task "G" ${WFL_HAR_DIR}/harness_G.sh
    launch_mi_task "K" ${WFL_HAR_DIR}/harness_K.sh 3
}


# Define all successors, called after any task completes
task_succeeded() {
    local succeeded=${1}

    # Sequence: After A, start B
    if completes_task ${succeeded} "A" ; then
        launch_task "B" ${WFL_HAR_DIR}/harness_B.sh
    fi

    # Fork: After C, start D and E
    if completes_task ${succeeded} "C" ; then
        launch_task "D" ${WFL_HAR_DIR}/harness_D.sh
        launch_task "E" ${WFL_HAR_DIR}/harness_E.sh
    fi

    # Join: After both D and E, start F
    if completes_join ${succeeded} "D" "E" ; then
        launch_task "F" ${WFL_HAR_DIR}/harness_F.sh
    fi

    # Split: After G, start either H or I depending on condition
    # (parity of cksum of $WF_ID))
    if completes_task ${succeeded} "G" ; then
        id_cksum=$(cksum <<< ${WF_ID} | cut -f 1 -d ' ')
        if [ 0 == $((${id_cksum} % 2)) ] ; then
            launch_task "H" ${WFL_HAR_DIR}/harness_H.sh
        else
            launch_task "I" ${WFL_HAR_DIR}/harness_I.sh
        fi
    fi

    # Merge: After either H or I, start J
    if completes_merge ${succeeded} "H" "I" ; then
        launch_task "J" ${WFL_HAR_DIR}/harness_J.sh
    fi

    # Sequence after multiple instance: After all of Ks, start L
    if completes_task ${succeeded} "K" ; then
        launch_task "L" ${WFL_HAR_DIR}/harness_L.sh
    fi
}
